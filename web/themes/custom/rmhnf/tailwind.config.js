module.exports = {
  mode: 'jit',
  content: [
    './templates/**/*.html.twig',
    './templates/*.html.twig',
    './components/**/*.twig',
    './components/**/*.stories.json',
    './components/**/*.stories.yml'
  ],
  theme: {
    minHeight: {
     '220': '220px',
     '115': '115px',
     '72': '72px'
    }
  }
}
